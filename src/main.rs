pub mod chess;
use chess::board::Board;
use chess::ui::UI;

fn main() {
	let mut board = Board::new();
	let mut ui = UI::new(&mut board);
}
