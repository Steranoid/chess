use chess::board::Board;

pub struct UI<'a> {
	board : &'a Board,
}

impl<'a> UI<'a> {
	pub fn new(board: &'a mut Board) -> UI<'a> {
		UI {
			board,
		}
	}
}
